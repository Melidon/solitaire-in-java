package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WasteTest {

	Waste waste;

	@Before()
	public void setup() {
		this.waste = new Waste();
		this.waste.merge(Pile.newDeck());
	}

	@Test
	public void canMergeTest() {
		Assert.assertFalse("You should not be allowed to put a pile on top of the waste!",
				this.waste.canMerge(new Pile()));
	}

	@Test
	public void canSplitTest() {
		Assert.assertFalse("You should not be allowed to take away more then one card form the waste!",
				this.waste.canSplit(0));
		Assert.assertTrue("You should be allowed to take away one card form the waste!",
				this.waste.canSplit(this.waste.size() - 1));
	}

}
