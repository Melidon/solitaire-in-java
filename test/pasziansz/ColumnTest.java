package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ColumnTest {

	Column column;
	Pile deck;

	/**
	 * Since the addCard function is private, I have to be clever to create the test
	 * piles. :-)
	 */
	@Before
	public void setup() {
		this.column = new Column();
		this.deck = new Pile();
		this.deck.merge(Pile.newDeck());
		this.deck.turnOver(); // Now the top card should be an ace, that is facing up.
	}

	@Test
	public void canMergeTest1() {
		Pile king = this.deck.split(this.deck.size() - 13); // This is pile from king, queen, ... , two, ace of the same
															// suit.
		Pile notKing = king.split(1);
		Assert.assertTrue("You should be allowed to place a king on an empty column!", this.column.canMerge(king));
		Assert.assertFalse("You not should be allowed to place any card except a king on an empty column!",
				this.column.canMerge(notKing));
	}

	@Test
	public void canMergeTest2() {
		Pile blackKing = this.deck.split(this.deck.size() - 13);
		blackKing.split(1);
		this.column.merge(blackKing);
		Pile redQueen = this.deck.split(this.deck.size() - 12);
		redQueen.split(1);
		Assert.assertTrue("You should be allowed to place a lower card of the opposite color on a column!",
				this.column.canMerge(redQueen));
		Pile notRedQueen = this.deck.split(this.deck.size() - 1);
		Assert.assertFalse(
				"You should not be allowed to place any card on a column except for a lower card of the opposite color!",
				this.column.canMerge(notRedQueen));
	}

	@Test
	public void canSplitTest() {
		Pile blackKing = this.deck.split(this.deck.size() - 13);
		blackKing.split(1);
		this.column.merge(blackKing);
		Assert.assertTrue("You should be allowed to take an amount of cards from the top if all of them is facing up!",
				this.column.canSplit(0));
		this.column.get(0).turnOver();
		Assert.assertFalse("You should not be allowed to take away a card that is not facin up!",
				this.column.canSplit(0));
	}
}
