package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameTest {

	Game game;

	@Before
	public void setup() {
		this.game = new Game();
	}

	@Test
	public void stockSizeTest() {
		Assert.assertEquals("The stock size does not match!", 24, this.game.getStock().size());
	}

	@Test
	public void wasteSizeTest() {
		Assert.assertEquals("The waste size does not match!", 0, this.game.getWaste().size());
	}

	@Test
	public void tableauSizeTest() {
		Assert.assertEquals("The tableau size does not match!", 7, this.game.getTableau().length);
		for (int i = 0; i < this.game.getTableau().length; ++i) {
			Assert.assertEquals("A column size does not match!", i + 1, this.game.getTableau()[i].size());
		}
	}

	@Test
	public void foundationsSizeTest() {
		Assert.assertEquals("The foundations size does not match!", 4, this.game.getFoundations().length);
		for (Foundation foundation : this.game.getFoundations()) {
			Assert.assertEquals("A foundation size does not match!", 0, foundation.size());
		}
	}

	@Test
	public void cardFaceDirectionInStock() {
		for (int i = 0; i < this.game.getStock().size(); ++i) {
			Assert.assertFalse("All cards should face down in the stock", this.game.getStock().get(i).isFacingUp());
		}
	}

	@Test
	public void cardFaceDirectionInTableau() {
		for (int i = 0; i < this.game.getTableau().length; ++i) {
			for (int j = 0; j < this.game.getTableau()[i].size(); ++j) {
				if (j == this.game.getTableau()[i].size() - 1) {
					Assert.assertTrue("The top card should face up!", this.game.getTableau()[i].get(j).isFacingUp());
				} else {
					Assert.assertFalse("Cards not on the top shoul face down!",
							this.game.getTableau()[i].get(j).isFacingUp());
				}
			}
		}
	}

}
