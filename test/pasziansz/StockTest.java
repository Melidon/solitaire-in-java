package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StockTest {

	Stock stock;
	Waste waste;

	@Before
	public void setup() {
		this.stock = new Stock();
		this.stock.merge(Pile.newDeck());
		this.waste = new Waste();
		this.stock.setWaste(this.waste);
	}

	@Test
	public void canMergeTest() {
		Assert.assertFalse("You should not be allowed to put a pile on top of the stock!",
				this.stock.canMerge(new Pile()));
	}

	@Test
	public void canSplitTest() {
		Assert.assertFalse("You should not be allowed to take away any cards form the stock!", this.stock.canSplit(0));
	}

	@Test
	public void putOneCardToWasteTest() {
		int size = this.stock.size();
		this.stock.putOneCardToWaste();
		Assert.assertEquals("The size is not matching!", size - 1, this.stock.size());
		Assert.assertEquals("The size of the waste is not matching!", 1, this.waste.size());
	}

	@Test
	public void getBackWasteTest() {
		int size = this.stock.size();
		while (this.stock.size() > 0) {
			this.stock.putOneCardToWaste();
		}
		this.stock.getBackWaste();
		Assert.assertEquals("The size is not matching!", size, this.stock.size());
	}

}
