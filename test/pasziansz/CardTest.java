package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pasziansz.Card.Rank;
import pasziansz.Card.Suit;

public class CardTest {

	private static Rank rank = Rank.ACE;
	private static Suit suit = Suit.SPADES;
	private static String name = "A♠";

	Card card;

	@Before
	public void setup() {
		this.card = new Card(CardTest.rank, CardTest.suit);
	}

	@Test
	public void contructorTest() {
		Assert.assertEquals("The rank is not matching the given in the constructor!", CardTest.rank,
				this.card.getRank());
		Assert.assertEquals("The suit is not matching the given in the constructor!", CardTest.suit,
				this.card.getSuit());
		Assert.assertFalse("The card is not facing down!", this.card.isFacingUp());
	}

	@Test
	public void turnOverTest() {
		this.card.turnOver();
		Assert.assertTrue("The card is not facing up!", this.card.isFacingUp());
	}

	@Test
	public void toStringTest() {
		Assert.assertEquals("The name string does not match while facing down.", "??", this.card.toString());
		this.card.turnOver();
		Assert.assertEquals("The name string does not match while facing up.", CardTest.name, this.card.toString());
	}

}
