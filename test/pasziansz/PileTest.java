package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PileTest {

	Pile pile;

	@Before
	public void setup() {
		this.pile = Pile.newDeck();
	}

	@Test
	public void constructorTest() {
		Assert.assertEquals("The size does not match!", 52, this.pile.size());
		for (int i = 0; i < this.pile.size(); ++i) {
			Assert.assertFalse("All cards should face down in a new deck!", this.pile.get(i).isFacingUp());
		}
	}

	@Test
	public void turnOverTest() {
		Card bottom = this.pile.get(0);
		this.pile.turnOver();
		for (int i = 0; i < this.pile.size(); ++i) {
			Assert.assertTrue("All cards should face up after turnover!", this.pile.get(i).isFacingUp());
		}
		Assert.assertEquals("The bottom card should appear on top!", bottom, this.pile.get(this.pile.size() - 1));
	}

	@Test
	public void mergeTest() {
		this.pile.merge(Pile.newDeck());
		Assert.assertEquals("The size does not match after merge!", 104, this.pile.size());
	}

	@Test
	public void splitTest() {
		Pile pile2 = this.pile.split(26);
		Assert.assertEquals("The size does not match after split!", 26, this.pile.size());
		Assert.assertEquals("The size of the splitted part does not match after split!", 26, pile2.size());
	}

}
