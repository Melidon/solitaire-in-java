package pasziansz;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FoundationTest {

	Foundation foundation;
	Pile deck;

	/**
	 * Since the addCard function is private, I have to be clever to create the test
	 * piles. :-)
	 */
	@Before
	public void setup() {
		this.foundation = new Foundation();
		this.deck = new Pile();
		this.deck.merge(Pile.newDeck());
		this.deck.turnOver(); // Now the top card should be an ace, that is facing up.
	}

	@Test
	public void canMergeTest1() {
		Pile hand = this.deck.split(this.deck.size() - 1); // This is an ace
		Assert.assertTrue("You should be allowed to place aces on an empty foundation!",
				this.foundation.canMerge(hand));
		hand = this.deck.split(this.deck.size() - 1); // This is not an ace
		Assert.assertFalse("You not should be allowed to place any card except an ace on an empty foundation!",
				this.foundation.canMerge(hand));
	}

	@Test
	public void canMergeTest2() {
		Pile hand = this.deck.split(this.deck.size() - 1); // This is an ace
		this.foundation.merge(hand);
		hand = this.deck.split(this.deck.size() - 1); // This is a two of the same suit
		Assert.assertTrue("You should be allowed to place the next card of the same suit on a foundation!",
				this.foundation.canMerge(hand));
		hand = this.deck.split(this.deck.size() - 1); // This is a three
		Assert.assertFalse("You should not be allowed to place any card on the foundation except for the next card of the same suit!",
				this.foundation.canMerge(hand));
	}

	@Test
	public void canSplitTest() {
		for (int i = 0; i < 2; ++i) {
			Pile hand = this.deck.split(this.deck.size() - 1);
			this.foundation.merge(hand);
		}
		Assert.assertFalse("You should not be allowed to take away more then one card form the foundation!",
				this.foundation.canSplit(0));
		Assert.assertTrue("You should be allowed to take away one card form the foundation!",
				this.foundation.canSplit(this.foundation.size() - 1));
	}

}
