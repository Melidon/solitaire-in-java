package pasziansz;

/**
 * This pile is used, to represent the stock in solitaire.
 */
public class Stock extends Pile {

	private static final long serialVersionUID = 1L;

	/**
	 * The waste to where it can put cards, or get them back.
	 */
	private Waste waste;

	/**
	 * Sets the given waste to know where to put cards.
	 * 
	 * @param waste the waste to be set
	 */
	public void setWaste(Waste waste) {
		this.waste = waste;
	}

	/**
	 * Takes the top card, turns it over, and places it on the waste.
	 */
	public void putOneCardToWaste() {
		if (this.waste != null && this.size() > 0) {
			Pile topCard = this.split(this.size() - 1);
			topCard.get(0).turnOver();
			this.waste.merge(topCard);
		}
	}

	/**
	 * Takes all cards from the waste, turns them over, and places them on itself.
	 */
	public void getBackWaste() {
		if (this.waste != null && this.size() == 0) {
			Pile pile = this.waste.split(0);
			pile.turnOver();
			this.merge(pile);
		}
	}

	/**
	 * Returns false. A player can not place any pile on a stock.
	 * 
	 * @param pile the pile you want to place on top
	 * @return false always
	 */
	@Override
	public boolean canMerge(Pile pile) {
		return false;
	}

	/**
	 * Returns false. A player can not take away any cards from a stock.
	 * 
	 * @param index the index from where the player wants to get the cards.
	 * @return false always
	 */
	@Override
	public boolean canSplit(int index) {
		return false;
	}

}
