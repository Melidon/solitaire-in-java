package pasziansz;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

/**
 * This class is used to display a column in solitaire.
 */
public class ColumnGraphics extends PileGraphics {

	private static final long serialVersionUID = 1L;

	/**
	 * You can see the top 1/overlapRatio of a card when it is not on top of the
	 * column.
	 */
	private static Integer overlapRatio = 6;

	/**
	 * Reference to the column to display.
	 */
	private Column column;

	/**
	 * Constructs the column graphics class. It does not set a column to display,
	 * you have to do it manually.
	 */
	public ColumnGraphics() {
		super();
	}

	/**
	 * Sets the column to show.
	 * 
	 * @param column the column to show
	 */
	public void setColumn(Column column) {
		super.setPile(column);
		this.column = column;
		this.resetMinimumSize();
	}

	/**
	 * Recalculates the minimum required size for the column based on the in-game
	 * size of the cards, and the amount of the cards in the column.
	 */
	@Override
	public void resetMinimumSize() {
		this.setMinimumSize(
				new Dimension(PileGraphics.getCardSizeInGame().width, PileGraphics.getCardSizeInGame().height
						+ PileGraphics.getCardSizeInGame().height * (this.column.size() - 1) / overlapRatio));
	}

	/**
	 * Returns the overlap ratio.
	 * 
	 * @return the overlap ratio
	 */
	public static Integer getOverlapRatio() {
		return ColumnGraphics.overlapRatio;
	}

	/**
	 * Sets the overlap ratio
	 * 
	 * @param overlapRatio the new overlap ratio
	 */
	public static void setOverlapRatio(Integer overlapRatio) {
		ColumnGraphics.overlapRatio = overlapRatio;
	}

	/**
	 * Paints the column to the screen.
	 * 
	 * @param g it is essential
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Dimension currentSize = this.getSize();
		Dimension minimumSize = this.getMinimumSize();
		int x = (currentSize.width - minimumSize.width) / 2;
		int y = 0;
//		y = (currentSize.height - minimumSize.height) / 2;
		for (int i = 0; i < this.column.size(); ++i) {
			BufferedImage cardImage = PileGraphics.getCardImage(this.column.get(i));
			g.drawImage(cardImage, x, y + PileGraphics.getCardSizeInGame().height * i / overlapRatio,
					x + PileGraphics.getCardSizeInGame().width,
					y + PileGraphics.getCardSizeInGame().height
							+ PileGraphics.getCardSizeInGame().height * i / overlapRatio,
					0, 0, cardImage.getWidth(), cardImage.getHeight(), null);
		}
	}

	/**
	 * Calls the double clicked function of the game graphics with the column that
	 * this class is presenting.
	 * 
	 * @param e the mouse click event
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			PileGraphics.gameGraphics.doubleClicked(this.column);
		}
	}

	/**
	 * Sets the content, the position (offset included) of the hand of the player.
	 * 
	 * @param e the mouse pressed event with the location where it happened
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if (this.column.size() > 0) {
			int y = 0;
//			Dimension currentSize = this.getSize();
//			Dimension minimumSize = this.getMinimumSize();
//			y = (currentSize.height - minimumSize.height) / 2;
			int index = (e.getY() - y) * overlapRatio / PileGraphics.getCardSizeInGame().height;
			int maxIndex = this.column.size() - 1;
			if (index > maxIndex && index < maxIndex + overlapRatio) {
				index = maxIndex;
			}
			if (index == maxIndex && !this.column.get(maxIndex).isFacingUp()) {
				this.column.get(maxIndex).turnOver();
				PileGraphics.gameGraphics.repaint();
			} else if (this.column.canSplit(index)) {

				/*
				 * Setting the content of the hand.
				 */
				PileGraphics.gameGraphics.setHand(this.column.split(index), this.column);

				/*
				 * Setting the offset of the hand, that is the difference between the top left
				 * corner of the card, and the position where the player clicked.
				 */
				Point clickLocation = e.getPoint();
				Dimension currentSize = this.getSize();
				Dimension minimumSize = this.getMinimumSize();
				int ox = (currentSize.width - minimumSize.width) / 2;
				int oy = 0;
				ox = clickLocation.x - ox;
				oy = clickLocation.y - oy;
				oy -= index * PileGraphics.getCardSizeInGame().height / overlapRatio;
				Point offset = new Point(-ox, -oy);
				PileGraphics.gameGraphics.setHandPositionOffset(offset);

				/*
				 * Sets the position of the hand.
				 */
				Point location = this.getLocation();
				e.translatePoint(location.x, location.y);
				PileGraphics.gameGraphics.setHandPosition(e.getPoint());
				PileGraphics.gameGraphics.repaint();
			}
		}
	}

}
