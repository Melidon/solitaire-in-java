package pasziansz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * The file menu, where the player can start a new game, save the current game,
 * or load a saved one.
 */
public class FileMenu extends JMenu {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the gui.
	 */
	private GUI gui;

	/**
	 * The menu items.
	 */
	private JMenuItem newGame, save, loadSave;

	/**
	 * Constructs the file menu
	 * 
	 * @param gui the gui where the menu will be placed
	 */
	public FileMenu(GUI gui) {
		super("Fájl");
		this.gui = gui;
		this.build();
	}

	/**
	 * Builds the file menu.
	 */
	private void build() {
		this.newGame = new JMenuItem("Új játék");
		this.save = new JMenuItem("Játék mentése");
		this.loadSave = new JMenuItem("Játék betöltése");

		this.newGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gui.newGame();
			}
		});
		this.save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gui.saveGame();
			}
		});
		this.loadSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gui.loadGame();
			}
		});

		this.add(this.newGame);
		this.add(this.save);
		this.add(this.loadSave);
	}
}
