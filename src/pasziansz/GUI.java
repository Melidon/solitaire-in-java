package pasziansz;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * This is the main class of a solitaire game.
 */
public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * The game graphics to show on the screen.
	 */
	private GameGraphics gameGraphics;

	/**
	 * The settings of the game.
	 */
	private Settings settings;

	/**
	 * The menu system of the application.
	 */
	private Menu menu;

	/**
	 * Starts a new game.
	 */
	public void newGame() {
		this.gameGraphics.setGame(new Game());
	}

	/**
	 * Saves the current state of the game.
	 */
	public void saveGame() {
		Game game = this.gameGraphics.getGame();
		try {
			FileOutputStream fileOutputStream = new FileOutputStream("save.dat");
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(game);
			objectOutputStream.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * Loads back a saved game.
	 */
	public void loadGame() {
		Game game = null;
		try {
			FileInputStream fileInputStream = new FileInputStream("save.dat");
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			game = (Game) objectInputStream.readObject();
			objectInputStream.close();
		} catch (IOException | ClassNotFoundException exception) {
			exception.printStackTrace();
		}
		if (game != null) {
			this.gameGraphics.setGame(game);
		}
	}

	/**
	 * Saves and applies the setting on the game graphics.
	 */
	public void applySettings() {
		this.gameGraphics.applySettings();
		this.saveSettings();
	}

	/**
	 * Saves the current settings.
	 */
	private void saveSettings() {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream("settings.dat");
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(this.settings);
			objectOutputStream.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * Called at the start of the game. It loads the saved settings, or creates a
	 * new one, if it did not manage to load anything.
	 */
	private void loadSettings() {
		Settings loadedSettings = null;
		try {
			FileInputStream fileInputStream = new FileInputStream("settings.dat");
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			loadedSettings = (Settings) objectInputStream.readObject();
			objectInputStream.close();
		} catch (IOException | ClassNotFoundException exception) {
			loadedSettings = new Settings();
		}
		this.settings = loadedSettings;
	}

	/**
	 * Constructs the application with a new game and a menu.
	 */
	public GUI() {
		super();
		this.loadSettings();
		this.gameGraphics = new GameGraphics(this.settings);
		this.add(this.gameGraphics);
		this.menu = new Menu(this, this.settings);
		this.setJMenuBar(menu);
		this.pack();
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setTitle("Pasziánsz");
	}

}
