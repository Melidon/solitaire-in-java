package pasziansz;

import javax.swing.JMenuBar;

/**
 * The menu used to change the settings or save/load the game.
 */
public class Menu extends JMenuBar {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the gui.
	 */
	private GUI gui;

	/**
	 * Reference to the settings.
	 */
	private Settings settings;

	/**
	 * The file menu, where the player can start a new game, save the current game,
	 * or load a saved one.
	 */
	private FileMenu fileMenu;
	
	/**
	 * The settings menu where the player can change the settings of the game.
	 * 
	 * @see pasziansz.SettingsMenu
	 */
	private SettingsMenu settingsMenu;

	/**
	 * Constructs the menu.
	 * 
	 * @param gui the gui where the menu will be placed
	 * @param settings the settings that can be changed
	 */
	public Menu(GUI gui, Settings settings) {
		super();
		this.gui = gui;
		this.settings = settings;
		this.fileMenu = new FileMenu(this.gui);
		this.settingsMenu = new SettingsMenu(this.gui, this.settings);
		this.add(this.fileMenu);
		this.add(this.settingsMenu);
	}

}
