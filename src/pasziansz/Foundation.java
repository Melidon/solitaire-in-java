package pasziansz;

import pasziansz.Card.Rank;

/**
 * This pile is used, to represent one of the foundations in solitaire.
 */
public class Foundation extends Pile {

	private static final long serialVersionUID = 1L;

	/**
	 * Returns true if the given pile contains only one card, and it matches the
	 * suit of this foundation and its rank is one higher than the top card of the
	 * foundation. It also returns true, when the foundation is empty and the given
	 * card is an ace.
	 * 
	 * @param pile the pile you want to place on top
	 * @return true you can place the given pile on top of the foundation
	 */
	@Override
	public boolean canMerge(Pile pile) {
		if (pile == null || pile.size() != 1) {
			return false;
		}

		if (this.size() == 0) {
			return pile.cards.get(0).getRank() == Rank.ACE;
		} else {
			Card top = this.cards.get(this.size() - 1);
			Card newTop = pile.cards.get(0);

			boolean rank = top.getRank().ordinal() + 1 == newTop.getRank().ordinal();
			boolean suit = top.getSuit() == newTop.getSuit();

			return rank && suit;
		}
	}

	/**
	 * Returns true only if you want to get the top card from the foundation.
	 * 
	 * @param index from where you want to get the cards
	 * @return true if you wanted to get the top card
	 */
	@Override
	public boolean canSplit(int index) {
		return this.size() - 1 == index;
	}

}
