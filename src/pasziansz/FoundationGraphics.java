package pasziansz;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

/**
 * This class is used to display a foundation in solitaire.
 */
public class FoundationGraphics extends PileGraphics {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the foundation to display.
	 */
	private Foundation foundation;

	/**
	 * Constructs the foundation graphics class. It does not set a foundation to
	 * display, you have to do it manually.
	 */
	public FoundationGraphics() {
		super();
	}

	/**
	 * Sets the foundation to show.
	 * 
	 * @param foundation the foundation to show
	 */
	public void setFoundation(Foundation foundation) {
		super.setPile(foundation);
		this.foundation = foundation;
	}

	/**
	 * Paints the foundation to the screen.
	 * 
	 * @param g it is essential
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (this.foundation.size() > 0) {
			Dimension currentSize = this.getSize();
			Dimension minimumSize = this.getMinimumSize();
			int x = (currentSize.width - minimumSize.width) / 2;
			int y = (currentSize.height - minimumSize.height) / 2;
			BufferedImage cardImage = PileGraphics.getCardImage(this.foundation.get(this.foundation.size() - 1));
			g.drawImage(cardImage, x, y, x + PileGraphics.getCardSizeInGame().width,
					y + PileGraphics.getCardSizeInGame().height, 0, 0, cardImage.getWidth(), cardImage.getHeight(),
					null);
		}
	}

	/**
	 * Sets the content, the position (offset included) of the hand of the player.
	 * 
	 * @param e the mouse pressed event with the location where it happened
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		super.mousePressed(e);
		if (this.foundation.size() > 0) {
			PileGraphics.gameGraphics.setHand(this.foundation.split(this.foundation.size() - 1), this.foundation);
		}
	}

}
