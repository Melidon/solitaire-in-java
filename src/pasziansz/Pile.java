package pasziansz;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import pasziansz.Card.Rank;
import pasziansz.Card.Suit;

/**
 * This class can be used to represent a pile. If you want your pile to have
 * restrictions about placing and taking cards from it, you may want to inherit
 * from this class.
 */
public class Pile implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The cards in the pile.
	 */
	protected ArrayList<Card> cards;

	/**
	 * Constructs an empty pile.
	 */
	public Pile() {
		this.cards = new ArrayList<Card>();
	}

	/**
	 * Catenate the string representations of the cards in the pile, and returns it.
	 * 
	 * @return a the string representation of the pile
	 */
	@Override
	public final String toString() {
		String string = "";
		for (Card card : this.cards) {
			string = string.concat(card.toString() + ", ");
		}
		return string;
	}

	/**
	 * Creates a new deck with all possible cards in it.
	 * 
	 * The cards are ordered by suit, and than by rank.
	 * 
	 * @return the deck created
	 */
	public static Pile newDeck() {
		Pile deck = new Pile();
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				Card card = new Card(rank, suit);
				deck.addCard(card);
			}
		}
		return deck;
	}

	/**
	 * Turns over the whole pile.
	 * 
	 * It does it the same way, as you would do it with a real pile of cards, so the
	 * bottom card will appear on top.
	 */
	public void turnOver() {
		Collections.reverse(this.cards);
		for (int i = 0; i < this.size(); ++i) {
			this.cards.get(i).turnOver();
		}
	}

	/**
	 * Shuffles the pile.
	 */
	public void shuffle() {
		Collections.shuffle(this.cards);
	}

	/**
	 * Returns the number of cards in the pile.
	 *
	 * @return the number of cards in the pile
	 */
	public int size() {
		return this.cards.size();
	}

	/**
	 * Places a card on top of the pile.
	 * 
	 * @param card the card to placed on top of the pile
	 */
	private final void addCard(Card card) {
		this.cards.add(card);
	}

	/**
	 * Returns a card at the specified position in the pile from the bottom.
	 * 
	 * @param index the index of the card from the bottom of the pile
	 * @return the card at the specified position
	 */
	public final Card get(int index) {
		return this.cards.get(index);
	}

	/**
	 * Removes and returns the card at the specified position in the pile from the
	 * bottom.
	 * 
	 * @param index the index of the card - from the bottom - to be removed form the
	 *              pile
	 * @return the card that was removed form the pile
	 */
	private final Card remove(int index) {
		return this.cards.remove(index);
	}

	/**
	 * Places the given pile on top.
	 * 
	 * The given pile will be empty after the call of this function.
	 * 
	 * @param pile the pile to be placed on top
	 */
	public final void merge(Pile pile) {
		if (this == pile || pile == null) {
			return;
		}
		for (Card card : pile.cards) {
			this.addCard(card);
		}
		pile.cards.clear();
	}

	/**
	 * Returns true if this pile wants the given pile to be placed on top.
	 * 
	 * You may want to override this in any instances of pile.
	 * 
	 * @param pile the pile you want to place on top
	 * @return true if this pile wants the given pile to be placed on top
	 */
	public boolean canMerge(Pile pile) {
		if (pile == null) {
			return false;
		}
		return true;
	}

	/**
	 * Creates and returns a new pile from the cards at and above the specified
	 * index.
	 * 
	 * These cards will be removed from the original pile.
	 * 
	 * @param index the index from where the pile will be cut.
	 * @return the new pile that has been created.
	 */
	public final Pile split(int index) {
		Pile pile = new Pile();
		try {
			while (true) {
				Card card = remove(index);
				pile.addCard(card);
			}
		} catch (IndexOutOfBoundsException e) {

		}
		return pile;
	}

	/**
	 * Returns true if this pile wants to allow to be cut in "half" from the
	 * specified index.
	 * 
	 * You may want to override this in any instances of pile.
	 * 
	 * @param index the index from where the caller wants to cut the pile.
	 * @return true if the pile wants to allow to be cut in "half" from the
	 *         specified index
	 */
	public boolean canSplit(int index) {
		return true;
	}

}
