package pasziansz;

import java.io.Serializable;

/**
 * This class can be used to represent a card.
 */
public class Card implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Enumeration for the rank of a card.
	 */
	public enum Rank {
		ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;


		/**
		 * Returns a one character long representation of a rank.
		 * 
		 * @return a one character long representation
		 */
		@Override
		public String toString() {
			switch (this) {
			case ACE:
				return "A";
			case JACK:
				return "J";
			case QUEEN:
				return "Q";
			case KING:
				return "K";
			default:
				return Integer.toString(1 + this.ordinal());
			}
		}
	}

	/**
	 * Enumeration for the suit of a card.
	 */
	public enum Suit {
		SPADES, HEARTS, DIAMONDS, CLUBS;

		/**
		 * Enumeration for the color of a card/suit.
		 */
		public enum Color {
			BLACK, RED;
		}

		/**
		 * Returns the color of the suit.
		 * 
		 * @return the color of the suit
		 */
		public Color getColor() {
			if (this == CLUBS || this == SPADES) {
				return Color.BLACK;
			} else {
				return Color.RED;
			}
		}

		/**
		 * Returns the emblem of a suit.
		 * 
		 * @return the emblem of a suit
		 */
		@Override
		public String toString() {
			Character ch = '\u2660';
			int shift = this.ordinal() + (this.getColor() == Color.RED ? 4 : 0);
			ch = (char) (ch + shift);
			return ch.toString();
		}
	}

	private Rank rank;
	private Suit suit;
	private boolean facingUp;

	/**
	 * Constructs a card with the given parameters.
	 * 
	 * By default the card is facing down.
	 * 
	 * @param rank the rank of the card
	 * @param suit the suit of the card
	 */
	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
		this.facingUp = false;
	}

	/**
	 * Returns a two character long representation of a card.
	 * 
	 * It is ?? when the card is not facing up.
	 * 
	 * @return a two character long representation
	 */
	public String toString() {
		return this.facingUp ? this.rank.toString() + this.suit.toString() : "??";
	}

	/**
	 * Returns the rank of the card.
	 * 
	 * @return rank of the card.
	 */
	public Rank getRank() {
		return this.rank;
	}

	/**
	 * Returns the suit of the card.
	 * 
	 * @return suit of the card
	 */
	public Suit getSuit() {
		return this.suit;
	}

	/**
	 * Returns true if the card is facing up.
	 * 
	 * @return true if the card is facing up
	 */
	public boolean isFacingUp() {
		return this.facingUp;
	}

	/**
	 * Turns over the card.
	 */
	public void turnOver() {
		this.facingUp = !this.facingUp;
	}

}
