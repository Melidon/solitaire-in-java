package pasziansz;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * An abstract class used to display piles.
 */
public abstract class PileGraphics extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the game graphics to be able to pass some events, and to get the card images.
	 */
	protected static GameGraphics gameGraphics;
	
	/**
	 * Reference to the pile to display.
	 */
	private Pile pile;

	/**
	 * Constructs the pile graphics class. It does not set a pile to display, you
	 * have to do it manually.
	 */
	public PileGraphics() {
		this.setMinimumSize(PileGraphics.getCardSizeInGame());
		this.setBackground(null);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	
	public static void setGameGraphics(GameGraphics gameGraphics) {
		PileGraphics.gameGraphics = gameGraphics;
	}


	/**
	 * Returns the image of a given card based on the current settings.
	 * 
	 * @param card the card you want to get the image of
	 * @return the image of the specified card
	 */
	public static BufferedImage getCardImage(Card card) {
		if (!card.isFacingUp()) {
			return PileGraphics.gameGraphics.getCardBack();
		} else {
			BufferedImage cardFaces = PileGraphics.gameGraphics.getCardFaces();
			Dimension cardSize = new Dimension(cardFaces.getWidth() / 13, cardFaces.getHeight() / 4);
			int x = card.getRank().ordinal() * cardSize.width;
			int y = (3 - card.getSuit().ordinal()) * cardSize.height;
			return cardFaces.getSubimage(x, y, cardSize.width, cardSize.height);
		}
	}

	/**
	 * Resets the size of the panel.
	 */
	public void resetMinimumSize() {
		this.setMinimumSize(PileGraphics.getCardSizeInGame());
	}

	/**
	 * Returns the pile that this class is showing.
	 * 
	 * @return the pile that this class is showing
	 */
	public Pile getPile() {
		return this.pile;
	}

	/**
	 * Sets the pile to show.
	 * 
	 * @param pile the pile to show
	 */
	public void setPile(Pile pile) {
		this.pile = pile;
		this.setMinimumSize(PileGraphics.getCardSizeInGame());
	}

	/**
	 * Returns the in-game size of the cards.
	 * 
	 * @return the in-game size of the cards
	 */
	public static Dimension getCardSizeInGame() {
		return PileGraphics.gameGraphics.getSettings().getCardSizeInGame();
	}

	/**
	 * Useless.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Calculates and sets the position offset of the hand in the game graphics.
	 * 
	 * The offset is the difference from the position of the click, and the top left
	 * corner of the card.
	 * 
	 * @param e the event that contains the position of the click
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		Point clickLocation = e.getPoint();
		Dimension currentSize = this.getSize();
		Dimension minimumSize = this.getMinimumSize();
		int x = (currentSize.width - minimumSize.width) / 2;
		int y = (currentSize.height - minimumSize.height) / 2;
		x = clickLocation.x - x;
		y = clickLocation.y - y;
		Point offset = new Point(-x, -y);
		PileGraphics.gameGraphics.setHandPositionOffset(offset);
	}

	/**
	 * It forwards the mouse released event to the game graphics.
	 * 
	 * @param e the mouse released event
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		Point location = this.getLocation();
		e.translatePoint(location.x, location.y);
		PileGraphics.gameGraphics.mouseReleased(e);
	}

	/**
	 * Useless.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Useless.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Sets the position of the hand in the game graphics.
	 * 
	 * @param e the mouse dragged event
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		Point location = this.getLocation();
		e.translatePoint(location.x, location.y);
		PileGraphics.gameGraphics.setHandPosition(e.getPoint());
	}

	/**
	 * Useless.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
	}

}
