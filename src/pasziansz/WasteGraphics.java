package pasziansz;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

/**
 * This class is used to display a waste in solitaire.
 */
public class WasteGraphics extends PileGraphics {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the waste to display.
	 */
	private Waste waste;

	/**
	 * Constructs the waste graphics class. It does not set a waste to display, you
	 * have to do it manually.
	 */
	public WasteGraphics() {
		super();
	}

	/**
	 * Sets the waste to show.
	 * 
	 * @param waste the waste to show
	 */
	public void setWaste(Waste waste) {
		super.setPile(waste);
		this.waste = waste;
	}

	/**
	 * Paints the waste to the screen.
	 * 
	 * @param g it is essential
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (this.waste.size() > 0) {
			Dimension currentSize = this.getSize();
			Dimension minimumSize = this.getMinimumSize();
			int x = (currentSize.width - minimumSize.width) / 2;
			int y = (currentSize.height - minimumSize.height) / 2;
			BufferedImage cardImage = PileGraphics.getCardImage(this.waste.get(this.waste.size() - 1));
			g.drawImage(cardImage, x, y, x + PileGraphics.getCardSizeInGame().width,
					y + PileGraphics.getCardSizeInGame().height, 0, 0, cardImage.getWidth(), cardImage.getHeight(),
					null);
		}
	}

	/**
	 * Calls the double clicked function of the game graphics with the waste that
	 * this class is presenting.
	 * 
	 * @param e the mouse click event
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			PileGraphics.gameGraphics.doubleClicked(this.waste);
		}
	}

	/**
	 * Sets the content, the position (offset included) of the hand of the player.
	 * 
	 * @param e the mouse pressed event with the location where it happened
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		super.mousePressed(e);
		if (this.waste.size() > 0) {
			PileGraphics.gameGraphics.setHand(this.waste.split(this.waste.size() - 1), this.waste);
		}
	}

}
