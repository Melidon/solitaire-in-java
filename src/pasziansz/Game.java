package pasziansz;

import java.io.Serializable;

/**
 * This class is used to represent the data structure of a game in solitaire.
 */
public class Game implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The stock in the game.
	 */
	private Stock stock;
	
	/**
	 * The waste in the game.
	 */
	private Waste waste;
	
	/**
	 * The tableau in the game that contains 7 columns.
	 */
	private Column[] tableau;
	
	/**
	 * The foundations in the game that contains 4 foundation.
	 */
	private Foundation[] foundations;

	/**
	 * @return
	 */
	public Stock getStock() {
		return stock;
	}

	/**
	 * Returns the waste in the game.
	 * 
	 * @return the waste in the game.
	 */
	public Waste getWaste() {
		return waste;
	}

	/**
	 * Returns the tableau in the game.
	 * 
	 * @return the tableau in the game.
	 */
	public Column[] getTableau() {
		return tableau;
	}

	/**
	 * Returns the foundations in the game.
	 * 
	 * @return the foundations in the game.
	 */
	public Foundation[] getFoundations() {
		return foundations;
	}

	/**
	 * Constructs a game with everything ready to play.
	 */
	public Game() {
		this.tableau = new Column[7];
		this.foundations = new Foundation[4];
		this.newGame();
	}

	/**
	 * Returns the string representation of the game.
	 * 
	 * @return the string representation of the game
	 */
	@Override
	public String toString() {
		String string = "Stock: " + this.stock.toString() + "\nWaste: " + this.waste.toString();
		for (int i = 0; i < 7; ++i) {
			string = string.concat("\nColumn[" + i + "]: " + this.tableau[i].toString());
		}
		for (int i = 0; i < 4; ++i) {
			string = string.concat("\nFoundation[" + i + "]: " + this.foundations[i].toString());
		}
		return string;
	}

	/**
	 * Clears the "board", and sets everything ready to play a new game.
	 */
	public void newGame() {
		this.stock = new Stock();
		this.waste = new Waste();
		this.stock.setWaste(this.waste);
		for (int i = 0; i < 7; ++i) {
			this.tableau[i] = new Column();
		}
		for (int i = 0; i < 4; ++i) {
			this.foundations[i] = new Foundation();
		}
		Pile deck = Pile.newDeck();
		deck.shuffle();
		for (int i = 0; i < 7; ++i) {
			int size = deck.size();
			Pile part = deck.split(size - (i + 1));
			this.tableau[i].merge(part);
			this.tableau[i].get(i).turnOver();
		}
		this.stock.merge(deck);
	}

	/**
	 * Returns true if the player won the game.
	 * 
	 * @return true if the game is won
	 */
	public boolean isVictory() {
		for (Foundation foundation : this.foundations) {
			if (foundation.size() != 13) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns true if form now on it is obvious how to win the game.
	 * 
	 * @return true if it is obvious how to win the game
	 */
	public boolean isObviousWin() {
		if (this.stock.size() > 0) {
			return false;
		}
		if (this.waste.size() > 0) {
			return false;
		}
		for (Column column : this.tableau) {
			if (column.size() > 0 && !column.get(0).isFacingUp()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Makes an obvious step, which means it puts one card from the tableau to the
	 * foundations.
	 */
	public void makeAnObviousStep() {
		for (Column column : this.tableau) {
			if (makeAnObviousStepFrom(column)) {
				break;
			}
		}
	}

	/**
	 * Puts one card from the specified pile to the foundations, if it is possible.
	 * 
	 * @param pile the pile from where it will make an obvious step
	 * @return true if one card has been moved from the given pile to the
	 *         foundations
	 */
	public boolean makeAnObviousStepFrom(Pile pile) {
		Pile topCard;
		if (pile.size() > 0) {
			topCard = pile.split(pile.size() - 1);
			for (Foundation foundation : this.foundations) {
				if (foundation.canMerge(topCard)) {
					foundation.merge(topCard);
					return true;
				}
			}
			pile.merge(topCard);
		}
		return false;
	}
}
