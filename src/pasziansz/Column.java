package pasziansz;

import pasziansz.Card.Rank;

/**
 * This pile is used, to represent one of the columns on the tableau in
 * solitaire.
 */
public class Column extends Pile {

	private static final long serialVersionUID = 1L;

	/**
	 * Returns true if this column wants the given pile to be placed on top.
	 * 
	 * It is true when the column is empty, and the bottom card of the given pile is
	 * a king, or the rank is one lower and the color is opposite of the bottom card
	 * of the given pile, compared to the top card of this column.
	 * 
	 * @param pile the pile you want to place on top
	 * @return true if this column wants the given pile to be placed on top
	 */
	@Override
	public boolean canMerge(Pile pile) {
		if (pile == null) {
			return false;
		}
		if (this.size() == 0) {
			return pile.cards.get(0).getRank() == Rank.KING;
		} else {
			Card top = this.cards.get(this.size() - 1);
			Card bottom = pile.cards.get(0);

			boolean rank = top.getRank().ordinal() - 1 == bottom.getRank().ordinal();
			boolean color = top.getSuit().getColor() != bottom.getSuit().getColor();

			return rank && color;
		}
	}

	/**
	 * Returns true if all the cards at the specified index and above are facing up.
	 * 
	 * @param index index from where this function looks the cards
	 * @return true if all the cards at the specified index and above are facing up
	 */
	@Override
	public boolean canSplit(int index) {
		if (index >= this.size() || index < 0) {
			return false;
		}
		return this.cards.get(index).isFacingUp();
	}

}
