package pasziansz;

import java.awt.Color;
import java.awt.Dimension;
import java.io.Serializable;

/**
 * This class stores the settings of the user.
 */
public class Settings implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private CardFaceStyle cardFaceStyle;
	private CardBackStyle cardBackStyle;
	private Color backgroundColor;
	private Dimension cardSizeInGame;
	private Integer colunmOverlapRatio;

	public enum CardFaceStyle {
		CLASSIC, MODERN, CUSTOM;

		@Override
		public String toString() {
			switch (this) {
			case CLASSIC:
				return "Klasszikus";
			case MODERN:
				return "Modern";
			case CUSTOM:
				return "Egyedi";
			default:
				return null;
			}
		}
		
		public String getImageName() {
			switch (this) {
			case CLASSIC:
				return "classic_13x4x560x780.png";
			case MODERN:
				return "modern_13x4x560x780.png";
			case CUSTOM:
				return "custom_13x4x560x780.png";
			default:
				return null;
			}
		}
	}

	public enum CardBackStyle {
		RED, YELLOW, BROWN, PURPLE, BLUE1, BLUE2;

		@Override
		public String toString() {
			switch (this) {
			case RED:
				return "Piros";
			case YELLOW:
				return "Sárga";
			case BROWN:
				return "Barna";
			case PURPLE:
				return "Lila";
			case BLUE1:
				return "Kék";
			case BLUE2:
				return "Sötét kék";
			default:
				return null;
			}
		}
		
		public String getImageName() {
			switch (this) {
			case RED:
				return "Card-Back-01.png";
			case YELLOW:
				return "Card-Back-02.png";
			case BROWN:
				return "Card-Back-03.png";
			case PURPLE:
				return "Card-Back-04.png";
			case BLUE1:
				return "Card-Back-05.png";
			case BLUE2:
				return "Card-Back-06.png";
			default:
				return null;
			}
		}
	}

	public Settings() {
		this.cardFaceStyle = CardFaceStyle.CLASSIC;
		this.cardBackStyle = CardBackStyle.RED;
		this.backgroundColor = Color.green;
		this.cardSizeInGame = new Dimension(144, 192);
		this.colunmOverlapRatio = 6;
	}

	public CardFaceStyle getCardFaceStyle() {
		return this.cardFaceStyle;
	}

	public void setCardFaceStyle(CardFaceStyle cardFaceStyle) {
		this.cardFaceStyle = cardFaceStyle;
	}

	public CardBackStyle getCardBackStyle() {
		return this.cardBackStyle;
	}

	public void setCardBackStyle(CardBackStyle cardBackStyle) {
		this.cardBackStyle = cardBackStyle;
	}
	
	public Color getBackgroundColor() {
		return this.backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	public Dimension getCardSizeInGame() {
		return this.cardSizeInGame;
	}

	public void setCardSizeInGame(Dimension cardSizeInGame) {
		this.cardSizeInGame = cardSizeInGame;
	}

	public Integer getColunmOverlapRatio() {
		return this.colunmOverlapRatio;
	}

	public void setColunmOverlapRatio(Integer colunmOverlapRatio) {
		this.colunmOverlapRatio = colunmOverlapRatio;
	}

}
