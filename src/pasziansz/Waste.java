package pasziansz;

/**
 * This pile is used, to represent one of the waste in solitaire.
 */
public class Waste extends Pile {

	private static final long serialVersionUID = 1L;

	/**
	 * Returns false. A player can not place any pile on a waste.
	 * 
	 * @param pile the pile you want to place on top
	 * @return false always
	 */
	@Override
	public boolean canMerge(Pile pile) {
		return false;
	}

	/**
	 * Returns true if the player wants to get the top card.
	 * 
	 * @param index the index from where the player wants to get the cards.
	 * @return true if it points to the top card. Otherwise false.
	 */
	@Override
	public boolean canSplit(int index) {
		return this.size() - 1 == index;
	}

}
