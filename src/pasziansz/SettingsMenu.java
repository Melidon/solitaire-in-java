package pasziansz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * The settings menu where the player can change the settings of the game.
 * 
 * That means the option to change the face of the cards, the back of the cards,
 * the background color, the size of the cards, and the overlap ratio of the columns.
 * 
 * @see pasziansz.ColumnGraphics
 */
public class SettingsMenu extends JMenu {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the gui.
	 */
	private GUI gui;
	/**
	 * Reference to the settings.
	 */
	private Settings settings;

	/**
	 * The menu item where the user can change the face style of the cards.
	 */
	private CardFaceStyleMenu cardFaceStyleMenu;

	/**
	 * The menu item where the user can change the back of the cards.
	 */
	private CardBackStyleMenu cardBackStyleMenu;

	/**
	 * The menu item where the user can change the background color.
	 */
	private BackgroundColorMenu backgroundColorMenu;

	/**
	 * The menu item where the user can change the size of the cards.
	 */
	private CardSizeInGameMenu cardSizeInGameMenu;

	/**
	 * The menu item where the user can change the overlap ratio of a column.
	 * 
	 * @see pasziansz.ColumnGraphics
	 */
	private ColunmOverlapRatioMenu colunmOverlapRatioMenu;

	/**
	 * Constructs the settings menu.
	 * 
	 * @param gui the gui where the menu will be placed
	 * @param settings the settings that can be changed
	 */
	public SettingsMenu(GUI gui, Settings settings) {
		super("Beállítások");
		this.gui = gui;
		this.settings = settings;
		this.build();
	}

	/**
	 * Builds the settings menu.
	 */
	private void build() {
		this.cardFaceStyleMenu = new CardFaceStyleMenu(this.gui, this.settings);
		this.cardBackStyleMenu = new CardBackStyleMenu(this.gui, this.settings);
		this.backgroundColorMenu = new BackgroundColorMenu(this.gui, this.settings);
		this.cardSizeInGameMenu = new CardSizeInGameMenu(this.gui, this.settings);
		this.colunmOverlapRatioMenu = new ColunmOverlapRatioMenu(this.gui, this.settings);

		this.add(this.cardFaceStyleMenu);
		this.add(this.cardBackStyleMenu);
		this.add(this.backgroundColorMenu);
		this.add(this.cardSizeInGameMenu);
		this.add(this.colunmOverlapRatioMenu);
	}

	/**
	 * Inner class where the user can change the face style of the cards.
	 */
	public class CardFaceStyleMenu extends JMenu {

		private static final long serialVersionUID = 1L;

		private GUI gui;
		private Settings settings;

		public CardFaceStyleMenu(GUI gui, Settings settings) {
			super("Kártyastílus");
			this.gui = gui;
			this.settings = settings;
			this.build();
		}

		private void build() {
			for (Settings.CardFaceStyle cardFaceStyle : Settings.CardFaceStyle.values()) {
				JMenuItem cardFaceStyleItem = new JMenuItem(cardFaceStyle.toString());
				cardFaceStyleItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						settings.setCardFaceStyle(cardFaceStyle);
						gui.applySettings();
					}
				});
				this.add(cardFaceStyleItem);
			}
		}
	}

	/**
	 * Inner class where the user can change the back of the cards.
	 */
	public static class CardBackStyleMenu extends JMenu {

		private static final long serialVersionUID = 1L;

		private GUI gui;
		private Settings settings;

		public CardBackStyleMenu(GUI gui, Settings settings) {
			super("Kártyahátlap");
			this.gui = gui;
			this.settings = settings;
			this.build();
		}

		private void build() {
			for (Settings.CardBackStyle cardBackStyle : Settings.CardBackStyle.values()) {
				JMenuItem cardBackStyleItem = new JMenuItem(cardBackStyle.toString());
				cardBackStyleItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						settings.setCardBackStyle(cardBackStyle);
						gui.applySettings();
					}
				});
				this.add(cardBackStyleItem);
			}
		}
	}

	/**
	 * Inner class where the user can change the background color.
	 */
	public static class BackgroundColorMenu extends JMenu {

		private static final long serialVersionUID = 1L;

		private GUI gui;
		private Settings settings;
		private ColorWithName[] colors;

		public BackgroundColorMenu(GUI gui, Settings settings) {
			super("Háttérszín");
			this.gui = gui;
			this.settings = settings;
			this.setupColors();
			this.build();
		}

		private void setupColors() {
			this.colors = new ColorWithName[13];
			this.colors[0] = new ColorWithName(Color.black, "Fekete");
			this.colors[1] = new ColorWithName(Color.blue, "Kék");
			this.colors[2] = new ColorWithName(Color.cyan, "Cián");
			this.colors[3] = new ColorWithName(Color.darkGray, "Sötét szürke");
			this.colors[4] = new ColorWithName(Color.gray, "Szürke");
			this.colors[5] = new ColorWithName(Color.green, "Zöld");
			this.colors[6] = new ColorWithName(Color.lightGray, "Világos szürke");
			this.colors[7] = new ColorWithName(Color.magenta, "Magenta");
			this.colors[8] = new ColorWithName(Color.orange, "Narancssárga");
			this.colors[9] = new ColorWithName(Color.pink, "Rózsaszín");
			this.colors[10] = new ColorWithName(Color.red, "Piros");
			this.colors[11] = new ColorWithName(Color.white, "Fehér");
			this.colors[12] = new ColorWithName(Color.yellow, "Sárga");
		}

		private void build() {
			for (ColorWithName backgroundColor : this.colors) {
				JMenuItem backgroundColorItem = new JMenuItem(backgroundColor.name);
				backgroundColorItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						settings.setBackgroundColor(backgroundColor.color);
						gui.applySettings();
					}
				});
				this.add(backgroundColorItem);
			}
		}

		public static class ColorWithName {

			public Color color;
			public String name;

			ColorWithName(Color color, String name) {
				this.color = color;
				this.name = name;
			}
		}
	}

	/**
	 * Inner class where the user can change the size of the cards.
	 */
	public static class CardSizeInGameMenu extends JMenu {

		private static final long serialVersionUID = 1L;

		private GUI gui;
		private Settings settings;
		private Dimension[] cardSizes;

		public CardSizeInGameMenu(GUI gui, Settings settings) {
			super("Kártyaméret");
			this.gui = gui;
			this.settings = settings;
			this.setupCardSizes();
			this.build();
		}

		private void setupCardSizes() {
			this.cardSizes = new Dimension[4];
			for (int i = 0; i < 4; ++i) {
				this.cardSizes[i] = new Dimension(36 * (i + 2), 48 * (i + 2));
			}
		}

		private void build() {
			for (Dimension cardSize : this.cardSizes) {
				JMenuItem cardSizeItem = new JMenuItem(cardSize.width + " x " + cardSize.height);
				cardSizeItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						settings.setCardSizeInGame(cardSize);
						gui.applySettings();
					}
				});
				this.add(cardSizeItem);
			}
		}
	}

	/**
	 * Inner class where the user can change the overlap ratio of a column.
	 */
	public static class ColunmOverlapRatioMenu extends JMenu {

		private static final long serialVersionUID = 1L;

		private GUI gui;
		private Settings settings;
		private Integer[] colunmOverlapRatios;

		public ColunmOverlapRatioMenu(GUI gui, Settings settings) {
			super("Átfedési arány");
			this.gui = gui;
			this.settings = settings;
			this.setupColumnOverlapRatios();
			this.build();
		}

		private void setupColumnOverlapRatios() {
			this.colunmOverlapRatios = new Integer[3];
			for (int i = 0; i < 3; ++i) {
				this.colunmOverlapRatios[i] = i + 4;
			}
		}

		private void build() {
			for (Integer colunmOverlapRatio : colunmOverlapRatios) {
				JMenuItem colunmOverlapRatioItem = new JMenuItem("1 : " + colunmOverlapRatio);
				colunmOverlapRatioItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						settings.setColunmOverlapRatio(colunmOverlapRatio);
						gui.applySettings();
					}
				});
				this.add(colunmOverlapRatioItem);
			}
		}
	}

}
