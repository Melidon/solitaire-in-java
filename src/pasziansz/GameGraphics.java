package pasziansz;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static javax.swing.GroupLayout.Alignment.*;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * This class is used to display a solitaire game.
 */
public class GameGraphics extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference for a game to show.
	 */
	private Game game;

	/**
	 * The graphical representation of the stock in a game.
	 */
	private StockGraphics stockGraphics;

	/**
	 * The graphical representation of the waste in a game.
	 */
	private WasteGraphics wasteGraphics;

	/**
	 * The graphical representation of the tableau in a game.
	 */
	private ColumnGraphics[] tableauGraphics;

	/**
	 * The graphical representation of the foundations in a game.
	 */
	private FoundationGraphics[] foundationsGraphics;

	/**
	 * The pile that is currently in the hands of the player.
	 */
	private Pile hand;

	/**
	 * The pile where the cards are placed back when the player tries to place the
	 * content of their hand to a pile where they could not.
	 */
	private Pile handSource;

	/**
	 * The current position where the hand should be drawn.
	 */
	private Point handPosition;

	/**
	 * The difference between the top left edge of the hand, and the point where the
	 * player grabbed it.
	 */
	private Point handPositionOffset;

	/**
	 * The settings of the game.
	 * 
	 * @see pasziansz.Settings
	 */
	private Settings settings;

	/**
	 * The image that contains the faces of the chosen card style.
	 */
	private BufferedImage cardFaces;

	/**
	 * The image that contains the back of the chosen card style.
	 */
	private BufferedImage cardBack;

	/**
	 * The timer that is used at the end of the game, when the cards are
	 * automatically moving to the foundation one by one.
	 */
	private Timer timer;

	/**
	 * Constructs the game graphics with the provided settings.
	 * 
	 * @param settings the settings for the graphics, it can not be null
	 */
	public GameGraphics(Settings settings) {
		this.addMouseMotionListener(this);
		this.settings = settings;
		PileGraphics.setGameGraphics(this);
		this.applySettings();
		this.stockGraphics = new StockGraphics();
		this.wasteGraphics = new WasteGraphics();
		this.tableauGraphics = new ColumnGraphics[7];
		for (int i = 0; i < 7; ++i) {
			this.tableauGraphics[i] = new ColumnGraphics();
		}
		this.foundationsGraphics = new FoundationGraphics[4];
		for (int i = 0; i < 4; ++i) {
			this.foundationsGraphics[i] = new FoundationGraphics();
		}
		this.setGame(new Game());

		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.stockGraphics)
						.addComponent(this.tableauGraphics[0]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.wasteGraphics)
						.addComponent(this.tableauGraphics[1]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.tableauGraphics[2]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.foundationsGraphics[0])
						.addComponent(this.tableauGraphics[3]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.foundationsGraphics[1])
						.addComponent(this.tableauGraphics[4]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.foundationsGraphics[2])
						.addComponent(this.tableauGraphics[5]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.foundationsGraphics[3])
						.addComponent(this.tableauGraphics[6])));
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.stockGraphics)
						.addComponent(this.wasteGraphics).addComponent(this.foundationsGraphics[0])
						.addComponent(this.foundationsGraphics[1]).addComponent(this.foundationsGraphics[2])
						.addComponent(this.foundationsGraphics[3]))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(this.tableauGraphics[0])
						.addComponent(this.tableauGraphics[1]).addComponent(this.tableauGraphics[2])
						.addComponent(this.tableauGraphics[3]).addComponent(this.tableauGraphics[4])
						.addComponent(this.tableauGraphics[5]).addComponent(this.tableauGraphics[6])));

	}

	/**
	 * Returns the game attribute.
	 * 
	 * It is used to save the game.
	 * 
	 * @return the game attribute
	 */
	public Game getGame() {
		return this.game;
	}

	/**
	 * Overrides the current game with the specified one.
	 * 
	 * It is used to load back a save.
	 * 
	 * @param game the game that will be played
	 */
	public void setGame(Game game) {
		this.game = game;
		this.stockGraphics.setStock(this.game.getStock());
		this.wasteGraphics.setWaste(this.game.getWaste());
		for (int i = 0; i < 7; ++i) {
			this.tableauGraphics[i].setColumn(this.game.getTableau()[i]);
		}
		for (int i = 0; i < 4; ++i) {
			this.foundationsGraphics[i].setFoundation(this.game.getFoundations()[i]);
		}
		this.repaint();
	}

	/**
	 * Returns the settings.
	 * 
	 * @return the settings
	 */
	public Settings getSettings() {
		return this.settings;
	}

	/**
	 * Applies the settings.
	 * 
	 * It is called when the settings are changed.
	 */
	public void applySettings() {
		this.loadTextures();
		this.setBackground(this.settings.getBackgroundColor());
		if (this.stockGraphics != null) {
			this.stockGraphics.resetMinimumSize();
		}
		if (this.wasteGraphics != null) {
			this.wasteGraphics.resetMinimumSize();
		}
		if (this.tableauGraphics != null) {
			for (ColumnGraphics column : this.tableauGraphics) {
				column.resetMinimumSize();
			}
		}
		if (this.foundationsGraphics != null) {
			for (FoundationGraphics foundation : this.foundationsGraphics) {
				foundation.resetMinimumSize();
			}
		}
		ColumnGraphics.setOverlapRatio(this.settings.getColunmOverlapRatio());
		if (this.tableauGraphics != null) {
			for (ColumnGraphics column : this.tableauGraphics) {
				column.resetMinimumSize();
			}
		}
		this.repaint();
	}

	/**
	 * Loads the textures of the cards to be shown.
	 */
	private void loadTextures() {
		try {
			this.cardFaces = ImageIO.read(new File("assets" + File.separator + "Sprite Sheets" + File.separator
					+ this.settings.getCardFaceStyle().getImageName()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			this.cardBack = ImageIO.read(new File("assets" + File.separator + "Backs" + File.separator
					+ this.settings.getCardBackStyle().getImageName()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BufferedImage getCardFaces() {
		return cardFaces;
	}

	public BufferedImage getCardBack() {
		return cardBack;
	}

	/**
	 * Puts a pile to the hand of the player. It also stores its source.
	 * 
	 * @param pile       this pile will be in the hand of the player
	 * @param sourcePile this pile will be used to put back the hand, when the
	 *                   player tried to place it somewhere and it was not
	 *                   successful
	 */
	public void setHand(Pile pile, Pile sourcePile) {
		this.hand = pile;
		this.handSource = sourcePile;
	}

	/**
	 * Returns the hand of the player
	 * 
	 * @return the hand of the player
	 */
	public Pile getHand() {
		return this.hand;
	}

	/**
	 * Puts the hand back to its source. It is called after the player tried to put
	 * the content of their hand and it was not successful
	 */
	public void putHandBackToSource() {
		if (this.hand != null && this.hand.size() > 0) {
			this.handSource.merge(this.hand);
		}
		this.repaint();
	}

	/**
	 * Sets the new position of the hand, and repaints the game.
	 * 
	 * @param point this will be the new position of the hand
	 */
	public void setHandPosition(Point point) {
		if (this.hand != null) {
			this.handPosition = point;
			this.repaint();
		}
	}

	/**
	 * Sets the offset of the position of the hand, which is the difference between
	 * the top left corner of the card, and the position where the player grabbed
	 * it.
	 * 
	 * @param offset
	 */
	public void setHandPositionOffset(Point offset) {
		this.handPositionOffset = offset;
	}

	/**
	 * Check if it is an obvious win. If that is the case, it puts the cards from
	 * the tableau to the foundations one by one.
	 */
	private void checkVictory() {
		if (this.game.isObviousWin() && this.timer == null) {
			this.timer = new Timer(256, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!game.isVictory()) {
						game.makeAnObviousStep();
						repaint();
					} else {
						timer.stop();
						timer = null;
					}
				}
			});
			timer.start();
		}
	}

	/**
	 * PileGraphics calls it, when it got double clicked, to try to make an obvious
	 * step.
	 * 
	 * @param pile the pile from where it tries to make an obvious step
	 */
	public void doubleClicked(Pile pile) {
		this.game.makeAnObviousStepFrom(pile);
		this.repaint();
	}

	/**
	 * Paints the current state of the game to the screen.
	 * 
	 * @param g it is essential
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (this.hand != null && this.hand.size() > 0) {
			Dimension cardSizeInGame = this.settings.getCardSizeInGame();
			Integer overlapRatio = ColumnGraphics.getOverlapRatio();
			Point position = new Point(this.handPosition.x + this.handPositionOffset.x,
					this.handPosition.y + this.handPositionOffset.y);
			for (int i = 0; i < this.hand.size(); ++i) {
				BufferedImage cardImage = PileGraphics.getCardImage(this.hand.get(i));
				g.drawImage(cardImage, position.x, position.y + cardSizeInGame.height * i / overlapRatio,
						position.x + cardSizeInGame.width,
						position.y + cardSizeInGame.height + cardSizeInGame.height * i / overlapRatio, 0, 0,
						cardImage.getWidth(), cardImage.getHeight(), null);
			}
		}
	}

	/*
	 * Useless.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	/*
	 * Useless.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
	}

	/**
	 * Tries to place the hand of the player where the release happened. If it is
	 * unsuccessful it puts back to its source.
	 * 
	 * @param e the event that contains the position where the release happened
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		Point location = e.getPoint();
		Component component = this.getComponentAt(location);
		PileGraphics pileGraphics = null;
		if (component instanceof PileGraphics) {
			pileGraphics = (PileGraphics) component;
		}
		if (pileGraphics != null) {
			if (hand != null && hand.size() > 0) {
				if (pileGraphics.getPile().canMerge(this.hand)) {
					pileGraphics.getPile().merge(this.hand);
					pileGraphics.resetMinimumSize();
					this.repaint();
				} else {
					this.putHandBackToSource();
				}
			}
		} else {
			this.putHandBackToSource();
		}
		this.checkVictory();
	}

	/*
	 * Useless.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/*
	 * Calls putHandBackToSource, to put back the hand to its source.
	 * 
	 * @param e we don't need that
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		this.putHandBackToSource();
	}

	/*
	 * Calls the setHandPosition function with the position of the event.
	 * 
	 * @param e
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		this.setHandPosition(e.getPoint());
	}

	/*
	 * Useless.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
	}

}
