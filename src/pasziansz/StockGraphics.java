package pasziansz;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

/**
 * This class is used to display a stock in solitaire.
 */
public class StockGraphics extends PileGraphics {

	private static final long serialVersionUID = 1L;

	/**
	 * Reference to the stock to display.
	 */
	private Stock stock;

	/**
	 * Constructs the stock graphics class. It does not set a stock to display, you
	 * have to do it manually.
	 */
	public StockGraphics() {
		super();
	}

	/**
	 * Sets the stock to show.
	 * 
	 * @param stock the stock to show
	 */
	public void setStock(Stock stock) {
		super.setPile(stock);
		this.stock = stock;
	}

	/**
	 * Paints the stock to the screen.
	 * 
	 * @param g it is essential
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (this.stock.size() > 0) {
			Dimension currentSize = this.getSize();
			Dimension minimumSize = this.getMinimumSize();
			int x = (currentSize.width - minimumSize.width) / 2;
			int y = (currentSize.height - minimumSize.height) / 2;
			BufferedImage cardImage = PileGraphics.getCardImage(this.stock.get(this.stock.size() - 1));
			g.drawImage(cardImage, x, y, x + PileGraphics.getCardSizeInGame().width,
					y + PileGraphics.getCardSizeInGame().height, 0, 0, cardImage.getWidth(), cardImage.getHeight(),
					null);
		}
	}

	/**
	 * Puts the top card from the stock to the waste.
	 * 
	 * @param e the mouse pressed event with the location where it happened
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		super.mousePressed(e);
		if (this.stock.size() > 0) {
			this.stock.putOneCardToWaste();
		} else {
			this.stock.getBackWaste();
		}
		PileGraphics.gameGraphics.repaint();
	}

}
